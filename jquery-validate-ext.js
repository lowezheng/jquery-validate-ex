$.validateSetup({
    //禁止form提交
    sendForm: false,
    onBlur: true,
    eachValidField: function (event, status, options) {
        console.log("eachValidField:"+JSON.stringify(status));
    },
    eachInvalidField: function (event, status, options) {
        console.log("eachInvalidField:"+JSON.stringify(status));
    },
    invalid: function (event,status, options) {
        console.log("invalid:"+JSON.stringify(status));
    },
    valid: function (event,status, options) {
        console.log("valid:"+JSON.stringify(status));
    },
    //校验规则function
    conditional: {
        required: function (fieldValue, options) {
            return !!fieldValue;
        },
        number: function (fieldValue, options) {
            return fieldValue.replace(/^([+]?[0-9])|([-]?[0-9])+\d*$/g,'').length==0;
        }
    },
    //校验提示string
    description: {
        required: "必填",
        number: "必须是数值"
    },
    //校验文本预处理function
    prepare:{

    }
});
