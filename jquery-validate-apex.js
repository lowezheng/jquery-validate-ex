/* http://plugins.jquery.com/validate */
;(function (defaults, $, window, undefined) {

    var

        type = ['input:not([type]),input[type="color"],input[type="date"],input[type="datetime"],input[type="datetime-local"],input[type="email"],input[type="file"],input[type="hidden"],input[type="month"],input[type="number"],input[type="password"],input[type="range"],input[type="search"],input[type="tel"],input[type="text"],input[type="time"],input[type="url"],input[type="week"],textarea', 'select', 'input[type="checkbox"],input[type="radio"]'],

    // All field types
        allTypes = type.join(','),

        extend = {},

    // Method to validate each fields
        validateField = function (event, options) {

            var

            // Field status
                status = {
                    conditional: true,
                    error: {}
                },

            // Current field
                field = $(this),

            // Current field value
                fieldValue = field.val() || '',

            // An index of extend
                fieldConditional = field.data('validate'),

            // One index or more separated for spaces to prepare the field value
                fieldPrepare = field.data('prepare'),

            //字段错误描述
                fieldDescription = field.data('description'),
            //字段中文
                fieldTitle = field.data('title'),

            // Is case sensitive? (Boolean)
                fieldIgnoreCase = field.data('ignoreCase'),

            // 去空格
                fieldTrim = !!field.data('trim'),

                reTrue = /^(true|)$/i,

                reFalse = /^false$/i,

                name = 'validate';


            // Trim spaces?
            if (reTrue.test(fieldTrim)) {

                fieldValue = $.trim(fieldValue);
            }

            // The fieldPrepare is a function?
            if ($.isFunction(fieldPrepare)) {

                // Updates the fieldValue variable
                fieldValue = String(fieldPrepare.call(field, fieldValue));
            } else {

                // Is a function?
                if ($.isFunction(options.prepare[fieldPrepare])) {

                    // Updates the fieldValue variable
                    fieldValue = String(options.prepare[fieldPrepare].call(field, fieldValue));
                }
            }

            // The conditional exists?
            if (fieldConditional != undefined) {

                // The fieldConditional is a function?
                if ($.isFunction(fieldConditional)) {

                    status.conditional = !!fieldConditional.call(field, fieldValue, options);
                } else {

                    var

                    // Splits the conditionals in an array
                        conditionals = fieldConditional.split(',');

                    // Each conditional
                    for (var counter = 0, len = conditionals.length; counter < len; counter++) {

                        if (options.conditional.hasOwnProperty(conditionals[counter])
                            && !options.conditional[conditionals[counter]].call(field, fieldValue, options)) {
                            var errorText = "错误未定义";
                            if(options.description.hasOwnProperty(conditionals[counter])){
                                errorText = options.description[conditionals[counter]]
                            }
                            status.conditional = false;
                            status.error[conditionals[counter]]=errorText;
                        }
                    }
                }
            }

            // Call the eachField callback
            options.eachField.call(field, event, status, options);

            //结果提示
            // If the field is valid
            if (status.conditional) {

                // Call the eachValidField callback
                options.eachValidField.call(field, event, status, options);
            } else {

                // Call the eachInvalidField callback
                options.eachInvalidField.call(field, event, status, options);
            }

            // Returns the field status
            return status;
        };

    $.extend({

        // Method to extends validations
        validateExtend: function (options) {

            return $.extend(extend, options);
        },

        // Method to change the default properties of jQuery.fn.validate method
        validateSetup: function (options) {

            return $.extend(defaults, options);
        }
    }).fn.extend({

        // Method to validate forms
        validate: function (options) {

            options = $.extend({}, defaults, options);

            return $(this).validateDestroy().each(function () {

                var form = $(this);

                // This is a form?
                if (form.is('form')) {

                    form.data(name, {
                        options: options
                    });

                    var

                        fields = form.find(allTypes),

                    // Events namespace
                        namespace = options.namespace;

                    if (form.is('[id]')) {

                        fields = fields.add('[form="' + form.prop('id') + '"]').filter(allTypes);
                    }

                    fields = fields.filter(options.filter);

                    // If onKeyup is enabled
                    if (!!options.onKeyup) {

                        fields.filter(type[0]).on('keyup.' + namespace, function (event) {

                            validateField.call(this, event, options);
                        });
                    }

                    // If onBlur is enabled
                    if (!!options.onBlur) {

                        fields.on('blur.' + namespace, function (event) {

                            validateField.call(this, event, options);
                        });
                    }

                    // If onChange is enabled
                    if (!!options.onChange) {

                        fields.on('change.' + namespace, function (event) {

                            validateField.call(this, event, options);
                        });
                    }

                    // If onSubmit is enabled
                    if (!!options.onSubmit) {

                        form.on('submit.' + namespace, function (event) {

                            var formValid = true;
                            var formStatus ={};

                            fields.each(function () {

                                var status = validateField.call(this, event, options);

                                if (!status.conditional) {

                                    formValid = false;
                                    formStatus[$(this).attr("name")]=status;
                                }
                            });

                            // If form is valid
                            if (formValid) {

                                // Send form?
                                if (!options.sendForm) {

                                    event.preventDefault();
                                }

                                // Is a function?
                                if ($.isFunction(options.valid)) {
                                    form.prop("formValid", true);
                                    options.valid.call(form, event,formStatus, options);
                                }
                            } else {

                                event.preventDefault();
                                event.stopImmediatePropagation();

                                // Is a function?
                                if ($.isFunction(options.invalid)) {
                                    form.prop("formValid", false);
                                    options.invalid.call(form, event,formStatus, options);
                                }
                            }
                        });
                    }
                }
            });
        },

        // Method to destroy validations
        validateDestroy: function () {

            var

                form = $(this),

                dataValidate = form.data(name);

            // If this is a form
            if (form.is('form') && $.isPlainObject(dataValidate) && typeof(dataValidate.options.nameSpace) == 'string') {

                var fields = form.removeData(name).find(allTypes).add(form);

                if (form.is('[id]')) {

                    fields = fields.add($('[form="' + form.prop('id') + '"]').filter(allTypes));
                }

                fields.off('.' + dataValidate.options.nameSpace);
            }

            return form;
        },
        // Method to validateForm validations
        validateForm: function () {

            var form = $(this);
            form.submit();
            return form.prop("formValid");
        }
    });
})({

    // Send form if is valid?
    sendForm: true,

    // Use WAI-ARIA properties
    waiAria: true,

    // Validate on submit?
    onSubmit: true,

    // Validate on onKeyup?
    onKeyup: false,

    // Validate on onBlur?
    onBlur: false,

    // Validate on onChange?
    onChange: false,

    // Default namespace
    nameSpace: 'validate',

    // Conditional functions
    conditional: {},

    // Prepare functions
    prepare: {},

    // Fields descriptions
    description: {},

    // Callback
    eachField: $.noop,

    // Callback
    eachInvalidField: $.noop,

    // Callback
    eachValidField: $.noop,

    // Callback
    invalid: $.noop,

    // Callback
    valid: $.noop,

    // A fielter to the fields
    filter: '*'
}, jQuery, window);